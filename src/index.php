<?php
declare(strict_types = 1);

/**
 * The function receives a value and outputs the result found in the file
 *
 * array['data']
 *          [0]     string  name of running php file
 *          [1]     string  csv file name
 *          [2]     string  a column number in which to search
 *          [3]     string  a search key
 *
 * @param array{phpFile:string, csvFile:string, column:string, value:string} $data
 *
 * @return string
 */
function searchInFile(array $data): string
{
    $result = '';
    $errorsOfVariables = checkVariable($data);

    if (empty($errorsOfVariables)) {
        $fileName = $data[1];
        $errorsOfFile = checkFile($fileName);

        if (empty($errorsOfFile)) {
            $file = fopen($fileName, 'rb');

            $result = searchValue($file, (int)$data[2], $data[3]);

            fclose($file);
        }
    }

    return !empty($errorsOfVariables) ? "\e[31m{$errorsOfVariables}\e[39m"  :
                (!empty($errorsOfFile) ? "\e[31m{$errorsOfFile}\e[39m"      :
                    ($result === '' ? "\e[33mNot Found\e[39m" : "\e[34mResult:\e[39m \r\n \e[32m{$result}\e[39m"));
}

/**
 * @param array $variables
 *
 * @return string
 */
function checkVariable(array $variables): string
{
    $errors = '';

    if (!isset($variables[1])) {
        $errors .= "Name of file doesn't exist\r\n";
    }
    if (!isset($variables[2])) {
        $errors .= "Colum's index doesn't exist\r\n";
    }
    if (!isset($variables[3])) {
        $errors .= "Value of searching doesn't exist\r\n";
    }

    return $errors;
}

/**
 * @param string $fileName
 *
 * @return string
 */
function checkFile(string $fileName): string
{
    $errors = '';

    if ((new SplFileInfo($fileName))->getExtension() !== "csv") {
        $errors .= "Your file doesn't have .csv extension\r\n";
    } elseif (!file_exists($fileName)) {
        $errors .= "File doesn't exist\r\n";
    } elseif (fopen($fileName, 'rb') === false) {
        $errors .= "Unable to read file\r\n";
    }
    return $errors;
}

/**
 * @param mixed $file
 * @param int $column
 * @param string $value
 *
 * @return string
 */
function searchValue(mixed $file, int $column, string $value): string
{
    $result = '';

    while (($data = fgetcsv($file, 0, ",")) !== FALSE) {
        $countOfColumns = count($data);

        if (strtolower($data[$column]) === strtolower($value)) {
            foreach ($data as $index => $item) {
                $result .= $index === $countOfColumns - 1 ? "$item\r\n" : "$item,";
            }
        }
    }
    return $result;
}

if (PHP_SAPI === 'cli') {
    echo searchInFile($argv) . "\r\n";
}